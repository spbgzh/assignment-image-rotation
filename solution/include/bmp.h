#ifndef BMP_H
#define BMP_H

#if defined(__MACH__)
#include <stdlib.h>
#else
#include <malloc.h>
#endif
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

// image
struct image;

struct pixel;

enum read_status
{
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum write_status
{
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status to_bmp(FILE *out, struct image const *img);

void free_image(struct image *img);

struct image *new_image(void);

struct image *constructor_image(uint64_t width, uint64_t height);
struct pixel *get_pixel(const struct image *img, size_t col, size_t row);
void set_pixel(const struct image *img, struct pixel* pixel, size_t col, size_t row);
uint64_t get_width(struct image const *source);
uint64_t get_height(struct image const *source);

#endif // BMP_H
