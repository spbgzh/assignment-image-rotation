#ifndef IO_H
#define IO_H

#include "bmp.h"

enum start_status {
    START_OK = 0,
    INVALID_PARAMETERS_ARGUMENTS,
    START_FAILED_TO_OPEN_INPUT_FILE,
    START_FAILED_TO_OPEN_OUTPUT_FILE
};

enum end_status {
    END_OK = 0,
    END_FAILED_TO_CLOSE_INPUT_FILE,
    END_FAILED_TO_CLOSE_OUTPUT_FILE
};

enum start_status init(int argc, char **argv, char **in_path, char **out_path, FILE **in_file, FILE **out_file);

enum end_status end_operation(FILE **in_file, FILE **out_file, struct image *pic1, struct image *pic2);


#endif // IO_H
