#include "../include/bmp.h"

// bmp
#pragma pack(push, 1) // struct对齐
struct bmp_header
{
    uint16_t bfType;          //文件类型，BMP格式的文件这两个字节是0x4D42，10进制就是19778，字符显示就是‘BM’
    uint32_t bfileSize;       //文件大小(包含这14字节)
    uint32_t bfReserved;      //保留字，不考虑 包含bfReserved1和bfReserved2各两个字节，必须设置为0
    uint32_t bOffBits;        // 4字节的偏移，表示从文件头到位图数据的偏移，
    uint32_t biSize;          //指定此结构体的长度，为40
    uint32_t biWidth;         //位图宽
    uint32_t biHeight;        //位图高
    uint16_t biPlanes;        //平面数，为1
    uint16_t biBitCount;      //采用颜色位数，可以是1，2，4，8，16，24，新的可以是32
    uint32_t biCompression;   //压缩方式，可以是0，1，2，最常用的就是0（BI_RGB），表示不压缩
    uint32_t biSizeImage;     //实际位图数据的大小
    uint32_t biXPelsPerMeter; // X方向分辨率
    uint32_t biYPelsPerMeter; // Y方向分辨率
    uint32_t biClrUsed;       //使用的颜色数，如果为0说明使用所有，表示默认值(2^颜色位数)
    uint32_t biClrImportant;  //重要颜色数，如果为0，则表示所有颜色都是重要的
};
#pragma pack(pop)

struct image
{
    uint64_t width, height;
    struct pixel *data;
};

struct pixel
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
} __attribute__((__packed__));

struct image *new_image(void)
{
    struct image *image = NULL;
    image = (struct image *)malloc(sizeof(struct image));
    return image;
}

struct image *constructor_image(uint64_t width, uint64_t height)
{
    struct image *image = NULL;
    image = (struct image *)malloc(sizeof(struct image));
    image->height = height;
    image->width = width;
    image->data = malloc(sizeof(struct pixel) * width * height);
    return image;
}

struct pixel *get_pixel(const struct image *img, size_t col, size_t row)
{
    return &(img->data[row * img->width + col]);
}
void set_pixel(const struct image *img, struct pixel *pixel, size_t col, size_t row)
{
    img->data[row * img->width + col] = *pixel;
}

uint64_t get_width(struct image const *source)
{
    return source->width;
}

uint64_t get_height(struct image const *source)
{
    return source->height;
}

void free_image(struct image *img)
{
    free(img->data);
    free(img);
}

static const uint16_t NUM_bf_Type = 0x4D42;
static const uint16_t NUM_bi_Bit_Count = 24;
static const uint32_t NUM_bi_Size = 40;
static const uint32_t DOUBLE_SIZE = 4;

bool header_reader(FILE *in, struct bmp_header *header);
bool fread_check(FILE *in, struct image const *img, uint64_t i);
bool fseek_check(FILE *in_out, size_t padding);
bool fwrite_check(FILE *out, void *data, size_t size, uint64_t value);

enum read_status check_bmp_correctness(struct bmp_header *header);
void constructor_bmp(struct image *img, struct bmp_header *header);
size_t calculate_padding(const uint32_t DS, const uint64_t width, const uint16_t NbBC);
struct bmp_header init_header(struct image const *img);

enum read_status from_bmp(FILE *in, struct image *img)
{
    if (!in || !img)
    {
        return READ_ERROR;
    }

    struct bmp_header header;
    if (header_reader(in, &header))
        return READ_ERROR;
    enum read_status read_state = check_bmp_correctness(&header);
    if (read_state != READ_OK)
        return read_state;
    constructor_bmp(img, &header);
    const size_t padding = calculate_padding(DOUBLE_SIZE, img->width, NUM_bi_Bit_Count); //计算偏移量
    for (uint64_t i = 0; i < img->height; i++)
    {
        if (fread_check(in, img, i) || fseek_check(in, padding))
        {
            img->height = img->width = -1;
            free(img->data);
            return READ_ERROR;
        }
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img)
{
    if (!out || !img)
    {
        return WRITE_ERROR;
    }

    const size_t padding = calculate_padding(DOUBLE_SIZE, img->width, NUM_bi_Bit_Count); //计算偏移量
    struct bmp_header header = init_header(img);

    if (fwrite_check(out, &header, sizeof(struct bmp_header), 1))
        return WRITE_ERROR;

    for (uint64_t i = 0; i < img->height; i++)
    {
        if (fwrite_check(out, img->data + i * img->width, sizeof(struct pixel), img->width) || fseek_check(out, padding))
            return WRITE_ERROR;
    }

    return WRITE_OK;
}

enum read_status check_bmp_correctness(struct bmp_header *header)
{
    if (header->bfType > NUM_bf_Type || header->bfType <= 0)
    {
        return READ_INVALID_SIGNATURE;
    }
    if (header->biBitCount != NUM_bi_Bit_Count)
    {
        return READ_INVALID_BITS;
    }
    if (header->biSize != NUM_bi_Size)
    {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

void constructor_bmp(struct image *img, struct bmp_header *header)
{
    img->height = header->biHeight;
    img->width = header->biWidth;
    img->data = malloc(sizeof(struct pixel) * img->width * img->height);
}

size_t calculate_padding(const uint32_t DS, const uint64_t width, const uint16_t NbBC)
{
    return DS - ((width * NbBC) >> 3) & 3;
}

uint32_t biSizeImage_calc(struct image const *img)
{
    return ((img->width * NUM_bi_Bit_Count + 31) >> 5)
           << 2 * img->height;
}

struct bmp_header init_header(struct image const *img)
{
    return (struct bmp_header){
        .bfType = NUM_bf_Type,
        .biSizeImage = biSizeImage_calc(img), // biSizeImage = bytesPerLine * imageSize
        .bOffBits = sizeof(struct bmp_header),
        .biSize = NUM_bi_Size,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = NUM_bi_Bit_Count,
        .bfileSize = biSizeImage_calc(img) + sizeof(struct bmp_header)};
}

bool header_reader(FILE *in, struct bmp_header *header)
{
    return fread(header, sizeof(struct bmp_header), 1, in) == 0;
}

bool fread_check(FILE *in, struct image const *img, uint64_t i)
{
    return fread((char *)img->data + i * img->width * sizeof(struct pixel), sizeof(struct pixel), img->width, in) != img->width;
}

bool fseek_check(FILE *in_out, size_t padding)
{
    return fseek(in_out, padding, SEEK_CUR) != 0;
}

bool fwrite_check(FILE *out, void *data, size_t size, uint64_t value)
{
    return fwrite(data, size, value, out) != value;
}
