#include "../include/io.h"

enum start_status init(int argc, char **argv, char **in_path, char **out_path, FILE **in_file, FILE **out_file)
{
    if (argc != 3)
    {
        return INVALID_PARAMETERS_ARGUMENTS;
    }
    *in_path = argv[1];
    *out_path = argv[2];

    *in_file = fopen(*in_path, "rbe");
    if (*in_file == NULL)
    {
        return START_FAILED_TO_OPEN_INPUT_FILE;
    }
    *out_file = fopen(*out_path, "wbe");
    if (*out_file == NULL)
    {
        return START_FAILED_TO_OPEN_OUTPUT_FILE;
    }
    return START_OK;
}

enum end_status end_operation(FILE **in_file, FILE **out_file, struct image *pic1, struct image *pic2)
{
    if (fclose(*in_file))
    {
        return END_FAILED_TO_CLOSE_INPUT_FILE;
    }
    if (fclose(*out_file))
    {
        return END_FAILED_TO_CLOSE_OUTPUT_FILE;
    }
    free_image(pic1);
    free_image(pic2);
    return END_OK;
}
