#include "../include/rotate.h"

struct image *rotate(struct image const *source)
{
    struct image *rotated_image = constructor_image(get_height(source), get_width(source));
    for (uint64_t i = 0; i < get_height(rotated_image); i++)
    {
        for (uint64_t j = 0; j < get_width(rotated_image); j++)
        {
            struct pixel* pixel = get_pixel(source, i, get_width(rotated_image) - j - 1);
            set_pixel(rotated_image, pixel, j, i);
        }
    }
    return rotated_image;
}
