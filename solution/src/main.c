#include "../include/bmp.h"
#include "../include/io.h"
#include "../include/rotate.h"

int main(int argc, char **argv)
{
    char *input_path = NULL;
    char *output_path = NULL;
    FILE *input_pFile = NULL;
    FILE *output_pFile = NULL;

    switch (init(argc, argv, &input_path, &output_path, &input_pFile, &output_pFile))
    {
    case INVALID_PARAMETERS_ARGUMENTS:
        fprintf(stderr, "Invalid parameters arguments\n");
        return 1;
    case START_FAILED_TO_OPEN_INPUT_FILE:
        fprintf(stderr, "Failed to open input file\n");
        return 1;
    case START_FAILED_TO_OPEN_OUTPUT_FILE:
        fprintf(stderr, "Failed to open output file\n");
        return 1;
    case START_OK:
        fprintf(stderr, "Initialization successful!\n");
        fprintf(stderr, "Source File Location: %s\n\n", input_path);
    }

    struct image *picture;
    picture = new_image();
    enum read_status r_status = from_bmp(input_pFile, picture);
    if (r_status)
    {
        fprintf(stderr, "READ_ERROR %u\n", r_status);
        return 1;
    }

    struct image *rotated_picture = rotate(picture);
    enum write_status w_status = to_bmp(output_pFile, rotated_picture);
    if (w_status)
    {
        fprintf(stderr, "WRITE_ERROR %u\n", w_status);
        return 1;
    }

    switch (end_operation(&input_pFile, &output_pFile, picture, rotated_picture))
    {
    case END_FAILED_TO_CLOSE_INPUT_FILE:
        fprintf(stderr, "Failed to close input file\n");
        return 1;
    case END_FAILED_TO_CLOSE_OUTPUT_FILE:
        fprintf(stderr, "Failed to close output file\n");
        return 1;
    case END_OK:
        fprintf(stderr, "Successfully ended the program!\n");
        fprintf(stderr, "Target File Location: %s\n\n", output_path);
    }
    return 0;
}
